/** Support for image rendering
 * @file
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "elinks.h"

#include "cache/cache.h"
#include "osdep/image.h"
#include "osdep/osdep.h"
#include "terminal/terminal.h"
#include "terminal/image.h"
#include "util/error.h"
#include "util/memory.h"
#include "util/string.h"


void
show_image(struct terminal *term, struct screen_image *image)
{
	struct string s;
	int fd;
	struct fragment *f;

#if 0
	if (!term->imgfiles[image->imgid]) {
		/* FIXME: We hardcode /tmp here. */
		term->imgfiles[image->imgid] = stracpy("/tmp/elinksimgXXXXXX");
		fd = mkstemp(term->imgfiles[image->imgid]);
	} else {
		fd = open(term->imgfiles[image->imgid], O_WRONLY);
	}
#else
	if (term->imgfiles[image->imgid]) {
		unlink(term->imgfiles[image->imgid]);
		mem_free(term->imgfiles[image->imgid]);
	}
	/* FIXME: We hardcode /tmp here. */
	term->imgfiles[image->imgid] = stracpy("/tmp/elinksimgXXXXXX");
	fd = mkstemp(term->imgfiles[image->imgid]);
#endif
	f = get_cache_fragment(image->cached);
	assert(f->offset == 0);
	safe_write(fd, f->data, f->length);
	close(fd);

	if (!init_string(&s))
		return;
	add_format_to_string(&s, "%d %d %d %d %d %d %d %d %d %s", image->imgid,
	                     image->pixbox.x, image->pixbox.y, image->pixbox.width, image->pixbox.height,
	                     image->crop_pixbox.x, image->crop_pixbox.y, image->crop_pixbox.width, image->crop_pixbox.height,
	                     term->imgfiles[image->imgid]);
	do_terminal_function(term, TERM_FN_IMG_DRAW, s.source);
	done_string(&s);
}

void
sync_images(struct terminal *term)
{
	do_terminal_function(term, TERM_FN_IMG_SYNC, "");
}



void
get_image_size(struct cache_entry *ce, int *w, int *h)
{
	int fd;
	struct fragment *f;
	unsigned char tmpfile[] = "/tmp/elinksimgXXXXXX";

	fd = mkstemp(tmpfile);
	f = get_cache_fragment(ce);
	assert(f->offset == 0);
	safe_write(fd, f->data, f->length);
	close(fd);

	image_size(tmpfile, w, h);

	unlink(tmpfile);
}
