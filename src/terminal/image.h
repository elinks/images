#ifndef EL__TERMINAL_IMAGE_H
#define EL__TERMINAL_IMAGE_H

#include "util/box.h"

struct cache_entry;
struct terminal;

struct screen_image {
	int imgid;
	struct box pixbox;
	struct box crop_pixbox;
	struct cache_entry *cached;
};

extern void show_image(struct terminal *term, struct screen_image *image);
extern void sync_images(struct terminal *term);

extern void get_image_size(struct cache_entry *ce, int *w, int *h);

#endif
