/* Image rendering. */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>

#include "elinks.h"

#include "osdep/image.h"

#ifndef CONFIG_OS_UNIX

struct image_ctx *
init_images(void)
{
	return NULL;
}

void
done_images(struct image_ctx *img)
{
}


void
slave_images_getres(struct image_ctx *img, int *xres, int *yres)
{
}

void
slave_render_image(struct image_ctx *img, int imgid, struct box *img_box, struct box *crop_box, unsigned char *file);
{
}

void
slave_move_image(struct image_ctx *img, int imgid, int x, int y)
{
}

void
slave_hide_image(struct image_ctx *img, int imgid)
{
}

void
slave_sync_images(struct image_ctx *img)
{
}


void
image_size(unsigned char *file, int *w, int *h)
{
}

void
slave_hide_image_region(struct image_ctx *img, struct box *region)
{
}

#endif
