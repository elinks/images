/* Image drawing. These are the actual slave-side routines. */

#ifndef EL__OSDEP_IMAGE_H
#define EL__OSDEP_IMAGE_H

/* These routines are not directly available from "normal" code. To request
 * image drawing, always use the terminal/image.h interface. */

#include "util/box.h"

/* This is the slave-side image rendering context. */
struct image_ctx {
	/* Screen resolution in pixels. */
	int xres, yres;

	/* Cached per-image information. */
	struct image_data {
		/* Bounding box of the image. */
		struct box box;
		unsigned char *file;
		int visible;
	} images[MAX_IMAGES];

	/* Pipe to the image drawing helper. */
	int pipe_to[2];
	int pipe_from[2];
};

extern struct image_ctx *init_images(); // NULL if N/A
extern void done_images(struct image_ctx *img);

extern void slave_images_getres(struct image_ctx *img, int *xres, int *yres);
extern void slave_render_image(struct image_ctx *img, int imgid, struct box *img_box, struct box *crop_box, unsigned char *file);
extern void slave_hide_image_region(struct image_ctx *img, struct box *region);
extern void slave_sync_images(struct image_ctx *img);

/* This function does not require the slave-specific context and it is
 * called directly from the master. */
extern void image_size(unsigned char *file, int *w, int *h);

#endif
