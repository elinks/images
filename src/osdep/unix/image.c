/* Image rendering. */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "elinks.h"

#include "osdep/image.h"
#include "osdep/osdep.h"
#include "util/error.h"

#ifdef CONFIG_OS_UNIX

void
image_helper(struct image_ctx *img, int xfd)
{
	close(0); close(1);
	dup2(img->pipe_to[0], 0); dup2(img->pipe_from[1], 1);
	close(img->pipe_to[0]); close(img->pipe_to[1]);
	close(img->pipe_from[0]); close(img->pipe_from[1]);
	execl(DEFAULT_IMGHELPER_CMD, DEFAULT_IMGHELPER_CMD, NULL);
}

struct image_ctx *
init_images(void)
{
	struct image_ctx *img = calloc(1, sizeof(*img));

	setenv("W3M_TTY", ttyname(0), 0);

	c_pipe(img->pipe_to);
	c_pipe(img->pipe_from);
	close(start_thread((void (*)(void *, int)) image_helper, img, 0));
	close(img->pipe_to[0]);
	close(img->pipe_from[1]);

	return img;
}

void
done_images(struct image_ctx *img)
{
	safe_write(img->pipe_to[1], "2;\n", 3);
	close(img->pipe_to[1]);
	close(img->pipe_from[0]);
	free(img);
}


void
slave_images_getres(struct image_ctx *img, int *xres, int *yres)
{
	FILE *p;

	p = popen(DEFAULT_IMGHELPER_CMD " -test", "r");
	if (!p) return; // XXX
	fscanf(p, "%d %d", &img->xres, &img->yres);
	pclose(p);

	if (xres) *xres = img->xres;
	if (yres) *yres = img->yres;
}

void
slave_render_image(struct image_ctx *img, int imgid, struct box *img_box, struct box *crop_box, unsigned char *file)
{
	unsigned char buf[4096];
	int sx, sy;

	if (img->images[imgid].file)
		free(img->images[imgid].file);
	img->images[imgid].file = strdup(file);
	copy_box(&img->images[imgid].box, img_box);
	img->images[imgid].visible = 1;

	sx = crop_box->x - img_box->x;
	sy = crop_box->y - img_box->y;

	snprintf(buf, sizeof(buf), "0;%d;%d;%d;%d;%d;%d;%d;%d;%d;%s\n", imgid,
	         crop_box->x, crop_box->y,
		 img_box->width, img_box->height,
		 sx, sy,
		 crop_box->width, crop_box->height,
		 img->images[imgid].file);
	safe_write(img->pipe_to[1], buf, strlen(buf));
}

void
slave_hide_image_region(struct image_ctx *img, struct box *region)
{
	unsigned char buf[4096];

	snprintf(buf, sizeof(buf), "6;%d;%d;%d;%d\n", region->x, region->y, region->width, region->height);
	safe_write(img->pipe_to[1], buf, strlen(buf));
}

void
slave_sync_images(struct image_ctx *img)
{
	/* XSync() */
	safe_write(img->pipe_to[1], "3;\n", 3);
	/* Return '\n' for synchronization */
	safe_write(img->pipe_to[1], "4;\n", 3);
	unsigned char nl;
	if (safe_read(img->pipe_from[0], &nl, 1) < 0) {
		INTERNAL("Communication with w3mimgdisplay failed.");
	}
}


void
image_size(unsigned char *file, int *w, int *h)
{
	unsigned char pcmd[4096];
	FILE *p;

	snprintf(pcmd, sizeof(pcmd), "%s -size %s", DEFAULT_IMGHELPER_CMD, file);
	p = popen(pcmd, "r");
	if (!p) return; // XXX
	fscanf(p, "%d %d", w, h);
	pclose(p);
}

#endif
